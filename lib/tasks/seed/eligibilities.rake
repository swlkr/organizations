# lib/tasks/seed/eligibilities.rake
namespace :seed do
  desc 'Add eligibilities to organizations for smoke testing'
  task eligibilities: :environment do
    names = ["Youth", "Seniors", "Ex-Offenders", "Refugees", "Veterans", "Children", "Abuse Survivors"]

    names.each do |name|
      Eligibility.create name: name
    end
  end
end
