# lib/tasks/seed/organization_eligibilities.rake
namespace :seed do
  desc 'Add eligibilities to organizations for smoke testing'
  task eligibilities: :environment do
    names = ["Youth", "Seniors", "Ex-Offenders", "Refugees", "Veterans", "Children", "Abuse Survivors"]

    Organization.find_each do |org|
      el_names = names.sample(1 + rand(names.count))
      els = Eligibility.where("name in (?)", el_names)

      els.each do |el|
        org.eligibilities << el
      end
    end
  end
end
