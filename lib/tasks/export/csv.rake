# lib/tasks/export/csv.rake

require "csv"

namespace :export do
  desc "Export a csv with organizations by eligibility and optionally include related objects"
  task :csv, [:eligibility, :include_relationships] => :environment do |t, args|
    # this is a really long function
    # I would normally split this up
    # but this behavior isn't anywhere else yet
    name = args[:eligibility]
    eligibilities = nil
    if name
      eligibilities = Eligibility.where("lower(name) = ?", name.downcase)
      eligibilities.each { |el| el.selected = true }
    end

    organizations = Organization.filter_by_eligibilities(eligibilities)

    # this can probably be reused but only here
    # the goal here is to get the maximum number of locations
    # of any of the organizations matching the filter
    # then flatten them in the final csv
    locations = Location
                .select("count(organization_id) as org_count")
                .where({id: organizations.map(&:id)})
                .group(:organization_id)
                .order("org_count DESC")

    location_columns = (1..locations.first.org_count).map { |num| "location_#{num}" }

    # similar to above
    org_els = Eligibility
              .joins(:organization_eligibilities)
              .select("count(organization_id) as org_count")
              .where({id: organizations.map(&:id)})
              .group(:organization_id)
              .order("org_count DESC")

    org_columns = (1..org_els.first.org_count).map { |num| "eligibility_#{num}" }

    attributes = Organization.attribute_names
    attributes << location_columns
    attributes << org_columns
    attributes = attributes.flatten

    # this could be tested later as a function
    # TODO test this
    csv_contents = CSV.generate do |csv|
      csv << attributes

      organizations.each do |organization|
        csv << attributes.map{ |attr|
          if attr.starts_with? "location_"
            _, num = attr.split(/_/)

            organization.locations.try(:[], num.to_i - 1).try(:decorate).try(:address)
          elsif attr.starts_with? "eligibility_"
            _, el_num = attr.split(/_/)

            organization.eligibilities.try(:[], el_num.to_i - 1).try(:name)
          else
            organization.send(attr)
          end
        }
      end
    end

    File.write("organizations.csv", csv_contents)
  end
end
