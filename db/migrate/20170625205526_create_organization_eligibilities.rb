class CreateOrganizationEligibilities < ActiveRecord::Migration
  def change
    create_table :organization_eligibilities do |t|
      t.integer :organization_id
      t.integer :eligibility_id

      t.timestamps
    end

    add_index :organization_eligibilities, [:organization_id, :eligibility_id], unique: true, name: "idx_organization_eligibilities_organization_id_eligibility_id"
  end
end
