Rails.application.routes.draw do
  root to: "organizations#index"

  resources :organizations, only: [:index, :show] do
    collection do
      get "filter"
    end
  end
end
