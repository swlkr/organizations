require "rails_helper"

describe Organization, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }

    it "validates uniqueness of name" do
      organization = FactoryGirl.build(:organization, name: "Food Pantry")
      expect(organization).to be_valid

      duplicate_site = FactoryGirl.create(:organization, name: "Food Pantry")
      expect(organization).to_not be_valid
    end

    it { is_expected.to validate_presence_of(:description) }

    it "filters organizations by eligibility" do
      org1 = FactoryGirl.create(:organization, name: "Zero Eligibilities")
      org2 = FactoryGirl.create(:organization_with_eligibilities)
      org2.eligibilities.each { |el| el.selected = true }

      orgs = Organization.filter_by_eligibilities(org2.eligibilities)

      expect(orgs.count).to eq(1)
    end

    it "returns all organizations when there are none selected" do
      org1 = FactoryGirl.create(:organization, name: "Zero eligibilities")
      org2 = FactoryGirl.create(:organization_with_eligibilities)

      orgs = Organization.filter_by_eligibilities(org2.eligibilities)

      expect(orgs.count).to eq(2)
    end

    it "returns all organizations when nil is passed" do
      org1 = FactoryGirl.create(:organization, name: "Zero eligibilities")
      org2 = FactoryGirl.create(:organization_with_eligibilities)

      orgs = Organization.filter_by_eligibilities(nil)

      expect(orgs.count).to eq(2)
    end

    it "returns only organizations with all of the selected eligibilities" do
      el = FactoryGirl.create(:eligibility)
      el2 = FactoryGirl.create(:eligibility)
      org1 = FactoryGirl.create(:organization, name: "Zero eligibilities")
      org2 = FactoryGirl.create(:organization)
      org2.eligibilities << el
      org2.eligibilities << el2
      org3 = FactoryGirl.create(:organization)
      org3.eligibilities << el
      org3.eligibilities << el2

      org4 = FactoryGirl.create(:organization)
      org4.eligibilities << el

      els = [el, el2].each { |e| e.selected = true }

      orgs = Organization.filter_by_eligibilities(els, {and: true})

      expect(orgs.any? { |o| o.id === org2.id }).to eq(true)
      expect(orgs.any? { |o| o.id === org3.id }).to eq(true)
      expect(orgs.any? { |o| o.id === org1.id }).to eq(false)
      expect(orgs.any? { |o| o.id === org4.id }).to eq(false)
    end
  end
end
