require 'rails_helper'

RSpec.describe Eligibility, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }

    it "validates uniqueness of name" do
      eligibility = FactoryGirl.build(:eligibility, name: "Veterans")
      expect(eligibility).to be_valid

      FactoryGirl.create(:eligibility, name: "Veterans")
      expect(eligibility).to_not be_valid
    end

    it "is not selected by default" do
      eligibility = FactoryGirl.build(:eligibility, name: "Refugees")

      expect(eligibility).not_to be_selected
    end

    it "is selected when set with a valid map" do
      eligibility = FactoryGirl.build(:eligibility, name: "Refugees")

      eligibility.set_selected({"Refugees" => true})

      expect(eligibility).to be_selected
    end

    it "is selected when given any case" do
      eligibility = FactoryGirl.build(:eligibility, name: "Refugees")

      eligibility.set_selected({"refugees" => true})

      expect(eligibility).to be_selected
    end

    it "is not selected when set with nil" do
      eligibility = FactoryGirl.build(:eligibility, name: "Refugees")

      eligibility.set_selected(nil)

      expect(eligibility).not_to be_selected
    end
  end
end
