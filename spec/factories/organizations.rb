FactoryGirl.define do
  factory :organization do
    sequence(:name) { |i| "Organization #{i}" }
    description "This is a generic description."

    factory :organization_with_eligibilities do
      after(:create) do |org|
        org.eligibilities << FactoryGirl.create(:eligibility)
        org.eligibilities << FactoryGirl.create(:eligibility)
      end
    end
  end
end
