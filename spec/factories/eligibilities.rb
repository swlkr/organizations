FactoryGirl.define do
  factory :eligibility do
    sequence(:name) { |i| "Eligibility #{i}" }
  end
end
