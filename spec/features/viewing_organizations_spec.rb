require "rails_helper"

describe "Viewing organizations" do
  scenario "viewing all organizations" do
    organization = FactoryGirl.create(:organization)
    location = FactoryGirl.create(:location, organization: organization)
    other_organization = FactoryGirl.create(:organization)

    visit root_path

    expect(page).to have_content(organization.name)
    expect(page).to have_content(other_organization.name)
    expect(page).to have_content(location.address)

    click_link organization.name
    expect(page).to have_content(organization.name)
    expect(page).not_to have_content(other_organization.name)
  end

  scenario "filtering organizations" do
    organization = FactoryGirl.create(:organization_with_eligibilities)
    other_organization = FactoryGirl.create(:organization)

    visit root_path

    find(:css, "#eligibilities_[value='#{organization.eligibilities.first.name}']").set(true)
    click_button("apply_filter")

    expect(page).to have_content(organization.name)
    expect(page).not_to have_content(other_organization.name)
  end

  scenario "apply empty filter to organizations" do
    organization = FactoryGirl.create(:organization_with_eligibilities)
    other_organization = FactoryGirl.create(:organization)

    visit root_path

    click_button("apply_filter")

    expect(page).to have_content(organization.name)
    expect(page).to have_content(other_organization.name)
  end

  scenario "filter organizations with and instead of or" do
    el = FactoryGirl.create(:eligibility)
    el2 = FactoryGirl.create(:eligibility)
    org1 = FactoryGirl.create(:organization, name: "Zero eligibilities")
    org2 = FactoryGirl.create(:organization)
    org2.eligibilities << el
    org2.eligibilities << el2
    org3 = FactoryGirl.create(:organization)
    org3.eligibilities << el
    org3.eligibilities << el2

    org4 = FactoryGirl.create(:organization)
    org4.eligibilities << el

    visit root_path

    find(:css, "#eligibilities_[value='#{el.name}']").set(true)
    find(:css, "#eligibilities_[value='#{el2.name}']").set(true)
    find(:css, "#and").set(true)
    click_button("apply_filter")

    expect(page).to have_content(org2.name)
    expect(page).to have_content(org3.name)
    expect(page).not_to have_content(org1.name)
    expect(page).not_to have_content(org4.name)
  end
end
