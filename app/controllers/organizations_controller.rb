class OrganizationsController < ApplicationController
  def index
    @eligibilities = Eligibility.order(:name)
    @eligibilities.each { |el| el.set_selected(params) }
    @and = params.key?(:and) && params[:and] == "true"

    @organizations = Organization.filter_by_eligibilities(@eligibilities, { and: @and })
  end

  def show
    @show_eligibilities = true
    @organization = Organization.find(params[:id]).decorate
  end

  def filter
    if params && params[:eligibilities]
      res = params[:eligibilities].map { |el| [el, "yes"] }.to_h
    else
      res = {}
    end

    res[:and] = params.key?(:and) && params[:and] == "and"
    res[:action] = "index"
    redirect_to res
  end
end
