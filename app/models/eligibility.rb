class Eligibility < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  has_many :organization_eligibilities
  has_many :organizations, through: :organization_eligibilities

  attr_accessor :selected

  validates :name, presence: true, uniqueness: true

  def set_selected(params)
    # make sure case insensitive keys work
    # hence downcase
    if params
      down_params = params.map {|k, v| [k.downcase, v] }.to_h
      @selected = down_params.key?(name.downcase)
    end
  end

  def selected?
    @selected || false
  end
end
