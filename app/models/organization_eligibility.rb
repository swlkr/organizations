class OrganizationEligibility < ActiveRecord::Base
  belongs_to :organization
  belongs_to :eligibility

  validates :organization, :eligibility, presence: true
end
