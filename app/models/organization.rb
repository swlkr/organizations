class Organization < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true

  has_many :locations
  has_many :organization_eligibilities
  has_many :eligibilities, through: :organization_eligibilities

  def self.filter_by_eligibilities(eligibilities, opts = {})
    selected_eligibilities = (eligibilities || []).select { |el| el.selected? }
    names = selected_eligibilities.map(&:name).map(&:downcase)
    opts = opts || {and: false}

    if !selected_eligibilities.empty?

      if opts[:and] == true
        where(id: Eligibility
                .joins(:organization_eligibilities)
                .select(:organization_id)
                .where("lower(name) in (?)", names)
                .group(:organization_id)
                .having("count(organization_id) = ?", names.length))
        .order(:name)
      else
        where(id: Eligibility
                  .joins(:organization_eligibilities)
                  .where("lower(name) in (?)", names)
                  .select(:organization_id))
        .order(:name)
      end
    else
      order(:name)
    end
  end
end
